cmake_minimum_required(VERSION 2.8.3)
project(client_ros)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  qt_build
)
set( qt_distro C:\\Qt\\5.10.1\\gcc_64\\)
set(CMAKE_PREFIX_PATH ${qt_distro})
file(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS src/*.cpp include/client_ros/*.hpp include/client_ros/*.h)
file(GLOB QT_RESOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} resources/*.qrc)
include_directories(include ${catkin_INCLUDE_DIRS})
#set(CMAKE_PREFIX_PATH "/home/amir/Qt5.10.1/5.10.1/Src/qtdeclarative/include/QtQuick")
#find_package(QtQuick)
#set(CMAKE_PREFIX_PATH "/home/amir/Qt5.10.1/5.10.1/Src/qtquickcontrols2/tests/auto/cmake")
#################################
find_package(Qt5Core REQUIRED)
find_package(Qt5Sql REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Quick REQUIRED)
#find_package(QtQuick)
find_package(Qt5Multimedia REQUIRED)
#set(CMAKE_PREFIX_PATH "home/amir/Qt5.10.1/5.10.1/Src/qtquickcontrols2")
#set(CMAKE_PREFIX_PATH $ENV{QTDIR} ${CMAKE_PREFIX_PATH})
#set(CMAKE_PREFIX_PATH "/opt/qt57/include")
#list(APPEND CMAKE_PREFIX_PATH "/home/amir/Qt5.10.1/5.10.1/Src/qtquickcontrols2/tests/auto/cmake")
#set(CMAKE_PREFIX_PATH "/home/amir/Qt5.10.1/5.10.1/Src/qtquickcontrols2/tests/auto/cmake")
#find_package(qtquickcontrols2 REQUIRED)
#find_package(Qt5QuickControls2 REQUIRED)
#find_package(Qt5QuickTemplates2 REQUIRED)

find_package(Qt5Qml REQUIRED)
foreach(c Core Sql Widgets Quick Multimedia Qml) 
    if(${Qt5${c}_FOUND})
        message(STATUS "Qt5${c} found!")
    endif()
endforeach()

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

#set configs
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

QT5_ADD_RESOURCES(QT_RESOURCES_CPP ${QT_RESOURCES})
QT5_WRAP_UI(QT_FORMS_HPP ${QT_FORMS})
QT5_WRAP_CPP(QT_MOC_HPP ${QT_MOC})
include_directories(
    ${Qt5Core_INCLUDE_DIRS}
    ${Qt5Gui_INCLUDE_DIRS}
    ${Qt5Quick_INCLUDE_DIRS}
    ${QtQuick_INCLUDE_DIRS}
    ${Qt5QuickControls2_INCLUDE_DIRS}
    ${Qt5Widgets_INCLUDE_DIRS}
    ${Qt5PrintSupport_INCLUDE_DIRS}
    ${Qt5Qml_INCLUDE_DIRS}
   # ./src
    ${Qt5Sql_INCLUDE_DIRS}
    ${Qt5Charts_INCLUDE_DIRS}
    ${Qt5Multimedia_INCLUDE_DIRS}
    ${QT_INCLUDE_DIR}
    )

add_definitions( -std=c++11 -fPIC)
add_definitions(${Qt5Widgets_DEFINITIONS} ${QtQml_DEFINITIONS} ${${Qt5Quick_DEFINITIONS}})

catkin_package(
 INCLUDE_DIRS
  include
  CATKIN_DEPENDS
  std_msgs
  roscpp

)
include_directories(include)
include_directories(${catkin_INCLUDE_DIRS})
include_directories(${Eigen_INCLUDE_DIRS})

#add_executable(clientclass src/clientclass.cpp
 # ${coreheaders}
  #  ${corecpps}
#)
#target_link_libraries(clientclass ${catkin_LIBRARIES})
#target_link_libraries(clientclass ${PROJECT_NAME}
 #       Qt5::Core
  #      Qt5::Widgets
   #     Qt5::Quick
    #    Qt5::Sql
     #   Qt5::Multimedia
        #Qt5::QuickControls2

      #  )
#add_dependencies(clientclass client_ros_generate_messages_cpp)

add_executable(client_ros_node src/client_ros_node.cpp
src/clientclass.cpp
include/client_ros/clientclass.h
    ${coreheaders}
    ${corecpps}
)
#qt5_use_modules(gui Quick Core)

target_link_libraries(client_ros_node ${catkin_LIBRARIES})
target_link_libraries(client_ros_node
        Qt5::Core
        Qt5::Widgets
        Qt5::Quick
        Qt5::Sql
        Qt5::Multimedia
  #      Qt5::QuickControls2
 #       Qt5::QuickTemplates2
       # Qt5::Charts

        )
add_dependencies(client_ros_node client_ros_generate_messages_cpp_qt_build)




qt5_add_resources(qml_QRC resources/qml.qrc)
qt5_use_modules(client_ros_node Quick Gui Core Widgets)

## Mark cpp header files for installation
install(DIRECTORY include/client_ros/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h"
)
