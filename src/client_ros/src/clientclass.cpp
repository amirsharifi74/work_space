#include <client_ros/clientclass.h>
#include <iostream>
#include <std_msgs/String.h>
#include <ros/ros.h>
clientClass::clientClass(QObject *parent) : QObject(parent)
{
    sub_client = nh.subscribe("server",1,&clientClass::recievedatagram,this);
    pub_client = nh.advertise<std_msgs::String>("client",1);
    // server_sub = nh.subscribe("client",1,&serverrqml::getDatagram,this);
    // server_pub = nh.advertise<std_msgs::String>("server",1);
}

qint16 clientClass::getcppInt()
{
    return m_cppint;
    std::clog<<"cppint recived";
}


void clientClass::setCppInt(qint16 &a)
{
    m_cppint = a ;
    dataok = true;
    emit cppIntChanged();
}



void clientClass::sendDatagram()
{
    while(ros::ok() && dataok == true){
        mark.insert("mark",QJsonValue::fromVariant(m_cppint));
        QJsonDocument doc(mark);
        //        std_str(doc.toJson(QJsonDocument::Compact));
        QString strJson(doc.toJson(QJsonDocument::Compact));
//        qDebug()<<strJson;

//        qDebug()<<doc;
        //Just for test JsonParse        auto doc2 = QJsonDocument::fromJson(strJson.toUtf8());
        //Qvariant        send_doc = doc.toVariant();
        //        qDebug()<<"doc is";
        //        qDebug()<<doc2;
        std::stringstream ss;
        std_msgs::String msg;
        //Qstring        std_str = send_doc.toString();
        //        qDebug()<<std_str; //the bug Is Here...
        ss << strJson.toStdString();
        msg.data = ss.str();
//        qDebug()<<"msg IS";
//        std::clog<<msg;
        pub_client.publish(msg);
        dataok=false;
        ros::spinOnce();
        //OK TESTED
    }
}

void clientClass::recievedatagram(const std_msgs::String::ConstPtr& msg1)
{
    qDebug()<<"Data recived";
    msgss_topic = msg1->data.c_str();
    qDebug()<<"test3";
    qDebug()<<msgss_topic;
    msgStr=msgss_topic.toString();
    QJsonDocument doc = QJsonDocument::fromJson(msgStr.toUtf8());
    //doc = msgss_topic.toJsonDocument();
    aveobj = doc.object();
    avevalue = aveobj.value(QString("average"));
    m_average = avevalue.toInt();
    qDebug()<<"ave IS"<<m_average;
//    setaverage(averageFromJson);
    ros::spinOnce();
}

void clientClass::setaverage(qint16 &b)
{
    m_average = b;
    emit averageChanged();
}

qint16 clientClass::getaverage()
{
    return m_average;
}



//void serverrqml::getDatagram(const std_msgs::String::ConstPtr& msg)
//{
//    msgs_topic=msg->data.c_str();
//    qDebug()<<msgs_topic;
//    msgStr = msgs_topic.toString();
//    QJsonDocument doc2 = QJsonDocument::fromJson(msgStr.toUtf8());
//    m_set= doc2.object();
//    m_value= m_set.value(QString("mark"));
//    m_fromClientInt = m_value.toInt();
//    sum+= m_fromClientInt;
//    m_count++;
//    m_averagenum = serverrqml::average(sum,m_count);
//    qDebug()<<"Datagram recivedddd"<<m_fromClientInt;
//    trueorfalse = true;
//sendDatagram();
//}
