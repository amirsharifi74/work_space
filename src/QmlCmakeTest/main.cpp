#include "src/message.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>
int main(int argc, char *argv[])
{
//QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QGuiApplication app(argc, argv);
	qmlRegisterType<Message>("com.mycompany.messaging", 1, 0, "Message");
	QQmlApplicationEngine engine;
	engine.load(QUrl("qrc:/main.qml"));
	if (engine.rootObjects().isEmpty())
	return -1;

	return app.exec();
}
